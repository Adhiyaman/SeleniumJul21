package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DeleteLeadPage extends ProjectMethods{
	public DeleteLeadPage() {
		PageFactory.initElements(driver, this);
	}
		
	@CacheLookup
	@FindBy(className ="smallSubmit")
	WebElement eleupdate;
	
	public ViewLeads clickupdate() {
		//WebElement eleupdate = locateElement("class", "smallSubmit");
		click(eleupdate);
		return new ViewLeads();
	}
	
}









