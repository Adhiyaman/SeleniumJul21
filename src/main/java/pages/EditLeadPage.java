package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	public EditLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(id ="updateLeadForm_companyName")
	WebElement eleCompanyName;
	
	@CacheLookup
	@FindBy(className ="smallSubmit")
	WebElement eleupdate;
	

	public EditLeadPage typeCompanyName(String data) {
		//WebElement eleCompanyName = locateElement("id", "updateLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}
	
	public ViewLeads clickupdate() {
		//WebElement eleupdate = locateElement("class", "smallSubmit");
		click(eleupdate);
		return new ViewLeads();
	}
	
}









