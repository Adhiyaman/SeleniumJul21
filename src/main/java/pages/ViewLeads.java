package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLeads extends ProjectMethods{
	public ViewLeads() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(linkText ="Edit")
	WebElement ViewLead;
	@CacheLookup
	@FindBy(linkText ="Delete")
	WebElement DeleteLeads;
	@CacheLookup
	@FindBy(linkText ="Duplicate Lead")
	WebElement DuplicateLeads;
	@CacheLookup
	@FindBy(linkText ="Merge Leads")
	WebElement MergeLeads;
	
	public EditLeadPage clickEditLeads() {
		//WebElement ViewLead = locateElement("linktext", "Edit");
		click(ViewLead);
		return new EditLeadPage();
	}
	public DeleteLeadPage clickDeleteLeads() {
			//WebElement DeleteLeads = locateElement("linktext", "Delete");
			click(DeleteLeads);
			return new DeleteLeadPage();
	}
	public DuplicateLeadPage clickDuplicateLeads(String title) {
		//WebElement DeleteLeads = locateElement("linktext", "Delete");
		click(DuplicateLeads);
		verifyTitle(title);
		return new DuplicateLeadPage();
}
	public MergeLeadPage clickMergeLeads() {
		//WebElement DeleteLeads = locateElement("linktext", "Delete");
		click(MergeLeads);
		return new MergeLeadPage();
}

	
}









