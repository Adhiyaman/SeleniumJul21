package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	public MyLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(linkText ="Create Lead")
	WebElement eleCreateLead;
	
	@CacheLookup
	@FindBy(linkText ="Find Leads")
	WebElement eleFindLead;
	@CacheLookup
	@FindBy(linkText ="Merge Leads")
	WebElement eleMergeLead;
	
	public CreateLeadPage clickCreateLead() {
		//WebElemeeleFindLeadnt eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	public FindLeadPage clickFindLead() {
		//WebElement = locateElement("linktext", "Find Leads");
		click(eleFindLead);
		return new FindLeadPage();
}
	public MergeLeadPage clickMergeLead() {
		//WebElemeeleFindLeadnt eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleMergeLead);
		return new MergeLeadPage();
}
}









