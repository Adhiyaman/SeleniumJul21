package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{
	public MergeLeadPage() {
		PageFactory.initElements(driver, this);
	}
		
	@CacheLookup
	@FindBy(xpath ="(//img[@alt='Lookup'])[1]")
	WebElement elefromlead;
	@CacheLookup
	@FindBy(name ="firstName")
	WebElement elefirstName;
	@CacheLookup
	@FindBy(xpath ="//button[text()='Find Leads']")
	WebElement eleFindLeads1;
	@CacheLookup
	@FindBy(xpath ="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eletolead1;
	@CacheLookup
	@FindBy(xpath ="(//img[@alt='Lookup'])[2]")
	WebElement eletolead;
	@CacheLookup
	@FindBy(linkText ="Merge")
	WebElement Mergelead;
	public MergeLeadPage clickfromlead() {
		//WebElement eleupdate = locateElement("class", "smallSubmit");
		click(elefromlead);
		switchToWindow(1);
		return this;
	}
	public MergeLeadPage typefirstName(String data) {
		//WebElement elePhonenumber = locateElement("name", "phoneNumber");
		type(elefirstName, data);
		return this;
	}
	public MergeLeadPage clickFindLeadButton1() {
		//WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
		click(eleFindLeads1);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
					}
		return this;
	}
		//String leadId = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));

public MergeLeadPage clickWithNoSnap() {
	//WebElement eleupdate = locateElement("class", "smallSubmit");
	click(eletolead1);
	switchToWindow(0);
	return this;
}
public MergeLeadPage clicktolead() {
	//WebElement eleupdate = locateElement("class", "smallSubmit");
	click(eletolead);
	switchToWindow(1);
	return this;
}

public MergeLeadPage clickWithNoSnap1() {
//WebElement eleupdate = locateElement("class", "smallSubmit");
click(Mergelead);
acceptAlert();
return this;
}
}

/*click(locateElement("xpath", "(//img[@alt='Lookup'])[1]"));
		switchToWindow(1);
		type(locateElement("name", "firstName"),fromLead);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		String leadId = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		click(locateElement("xpath", "(//img[@alt='Lookup'])[2]"));
		switchToWindow(1);
		type(locateElement("name", "firstName"),toLead);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		clickWithNoSnap(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		switchToWindow(0);
		clickWithNoSnap(locateElement("linktext", "Merge"));
		acceptAlert();
		click(locateElement("linktext", "Find Leads"));
		type(locateElement("xpath", "//input[@name='id']"),leadId);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		WebElement eleVerify = locateElement("class", "x-paging-info");
		verifyExactText(eleVerify, errorMag);
	*/









