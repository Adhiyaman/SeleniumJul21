package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
	public FindLeadPage() {
		PageFactory.initElements(driver, this);
	}
	@CacheLookup
	@FindBy(xpath ="//span[text()='Phone']")
	WebElement elePhone;
	
	@CacheLookup
	@FindBy(name ="phoneNumber")
	WebElement elePhonenumber;
	
	@CacheLookup
	@FindBy(xpath ="//span[text()='Email']")
	WebElement eleEmail;
	
	@CacheLookup
	@FindBy(name ="emailAddress")
	WebElement eleEmailid;
	
	@CacheLookup
	@FindBy(xpath ="//button[text()='Find Leads']")
	WebElement eleFindLeads;
	
	@CacheLookup
	@FindBy(xpath ="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a")
	WebElement eleFirstLeads;
	

	public FindLeadPage clickEmail() {
		//WebElement elePhone= locateElement("xpath", "//span[text()='Phone']");
		click(eleEmail);
		return this; 
	}
		public FindLeadPage typeEmailid(String data) {
			//WebElement eleEmail = locateElement("name", "phoneNumber");
			type(eleEmailid, data);
			return this;
		}
		public FindLeadPage clickPhone() {
			//WebElement elePhone= locateElement("xpath", "//span[text()='Phone']");
			click(elePhone);
			return this; 
		}
			public FindLeadPage typePhonenumber(String data) {
				//WebElement elePhonenumber = locateElement("name", "phoneNumber");
				type(elePhonenumber, data);
				return this;
			}
			
		
		public FindLeadPage clickFindLeadButton() {
			//WebElement eleFindLeads= locateElement("xpath", "//button[text()='Find Leads']");
			click(eleFindLeads);
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return this; 
		}
		String leadName = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]"));
		//String leadId = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		public ViewLeads clickFirstleads() {
			//WebElement eleFirstLeads= locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
			click(eleFirstLeads);
			return new ViewLeads(); 
		}
		
		
		
		
	}
	










