package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{
	public DuplicateLeadPage() {
		PageFactory.initElements(driver, this);
	}
		
	@CacheLookup
	@FindBy(className ="smallSubmit")
	WebElement eleupdate;
	
	public DuplicateLeadPage clickupdate() {
		//WebElement eleupdate = locateElement("class", "smallSubmit");
		click(eleupdate);
		return this;
	}
	
}









