package tests;



import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC004_DuplicateLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC004_DuplicateLead";
		testCaseDescription ="Duplicate a lead";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC004";
	}
	@Test(dataProvider="fetchData")
	public  void DuplicateLead(String email, String title )   {
		new MyHomePage()
		.clickLeads()
		.clickFindLead()
		.clickEmail()
		.typeEmailid(email)
		.clickFindLeadButton()
		.clickFirstleads()
		.clickDuplicateLeads(title)
		.clickupdate();
		
		
		
		
		
		
		
		
		/*click(locateElement("linktext", "Leads"));
		click(locateElement("linktext", "Find Leads"));
		click(locateElement("xpath", "//span[text()='Email']"));
		type(locateElement("name", "emailAddress"),email);
		click(locateElement("xpath", "//button[text()='Find Leads']"));
		Thread.sleep(1000);
		String leadName = getText(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a[1]"));
		click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
		click(locateElement("linktext", "Duplicate Lead"));
		verifyTitle(title);
		click(locateElement("name", "submitButton"));
		WebElement eleVerify = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(eleVerify, leadName);	*/
		
		
	}
	

}
